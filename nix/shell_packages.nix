{ pkgs }:

with pkgs;
[
  # rust environment
  gcc

  # needed by `cargo build --release`
  glibc

  # needed by git-gamble's tests
  bash
  shunit2
  coreutils

  # needed by git-gamble itself
  git

  # needed only to know the test coverage
  cargo-tarpaulin

  # needed only when releasing a new version
  cargo-release

  # needed only to check crates vulnerabilities
  cargo-audit

  # needed only to detect what can be improved
  cargo-bloat
  cargo-diet
  cargo-license

  # needed to generate documentation schema
  plantuml

  # usefull for refactoring session
  cargo-watch

  # needed by VSCode extension to format Nix files
  rnix-lsp

  # bash still usable in interactive mode
  bashInteractive
]
