#!/usr/bin/env sh

REAL_PATH=$(realpath "$0")
DIRECTORY_PATH=$(dirname "${REAL_PATH}")

. "${DIRECTORY_PATH}/../skip_lines_before_pattern.sh"

test_skip_lines_before_pattern() {
    PATTERN="---"
    EXPECTED_CONTENT="${PATTERN}

some expected content"

    ACTUAL_CONTENT=$(
        echo "
some ignored content


${EXPECTED_CONTENT}" |
            skip_lines_before_pattern "${PATTERN}"
    )

    assertEquals "contents are different" "${EXPECTED_CONTENT}" "${ACTUAL_CONTENT}"
}

. shunit2
