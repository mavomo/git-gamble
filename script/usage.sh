#!/usr/bin/env sh

PROJECT_ROOT=$(dirname "$(realpath $0)")/..

. "${PROJECT_ROOT}/script/display_file_between_bounds.sh"

display_file_between_bounds \
	"## How to use" \
	"^## When to use it ?" \
	<"${PROJECT_ROOT}/README.md" |
	sed 's/<!-- .* -->//' |
	head -n -1
