use std::fs::create_dir_all;
use std::fs::remove_file;
use std::fs::rename as move_file;
#[cfg(unix)]
use std::fs::set_permissions;
use std::io;
#[cfg(unix)]
use std::os::unix::fs::PermissionsExt;

use crate::fixture::TestHook;
use crate::fixture::TestRepository;

const HOOK_NAME: &str = "pre-gamble";

#[test_log::test]
fn run_hook_when_hook_file_is_executable() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;
	let test_hook = TestHook::init(&test_repository, HOOK_NAME)?;

	let command = test_repository.execute_cli_with(&["--pass", "--", "true"]);

	command.success();
	test_hook.assert_that_hook_has_been_run();
	Ok(())
}

#[test_log::test]
fn run_hook_respecting_git_config_hooks_path() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	let configured_hooks_path = test_repository.path().join("hooks");
	test_repository.set_config(&["core.hooksPath", configured_hooks_path.to_str().unwrap()]);
	create_dir_all(&configured_hooks_path)?;

	let test_hook = TestHook::init(&test_repository, HOOK_NAME)?;
	move_file(
		test_repository.hook_path(HOOK_NAME),
		configured_hooks_path.join(HOOK_NAME),
	)?;

	let command = test_repository.execute_cli_with(&["--pass", "--", "true"]);

	command.success();
	test_hook.assert_that_hook_has_been_run();
	Ok(())
}

#[test_log::test]
fn exit_when_a_hook_fail() -> io::Result<()> {
	let test_repository = TestRepository::init_dirty()?;

	test_repository.add_hook(HOOK_NAME, "false")?;

	let command = test_repository.execute_cli_with(&["--pass", "--", "true"]);

	command.failure();
	Ok(())
}

mod do_not_run_hook {
	use super::*;

	#[test_log::test]
	fn when_hook_file_does_not_exists() -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;

		let test_hook = TestHook::init(&test_repository, HOOK_NAME)?;
		remove_file(test_repository.hook_path(HOOK_NAME))?;

		let command = test_repository.execute_cli_with(&["--pass", "--", "true"]);

		command.success();
		test_hook.assert_that_hook_has_not_been_run();
		Ok(())
	}

	#[cfg(unix)]
	#[test_log::test]
	fn when_hook_file_is_not_executable() -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;

		let test_hook = TestHook::init(&test_repository, HOOK_NAME)?;
		// Owner: Read + Write
		const OWNER_READ_WRITE_PERMISSIONS: u32 = 0o600;
		set_permissions(
			test_repository.hook_path(HOOK_NAME),
			PermissionsExt::from_mode(OWNER_READ_WRITE_PERMISSIONS),
		)?;

		let command = test_repository.execute_cli_with(&["--pass", "--", "true"]);

		command.success();
		test_hook.assert_that_hook_has_not_been_run();
		Ok(())
	}

	#[test_log::test]
	fn when_dry_run() -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;
		let test_hook = TestHook::init(&test_repository, HOOK_NAME)?;

		let command = test_repository.execute_cli_with(&["--pass", "--dry-run", "--", "true"]);

		command.success();
		test_hook.assert_that_hook_has_not_been_run();
		Ok(())
	}

	#[test_log::test]
	fn when_no_verify() -> io::Result<()> {
		let test_repository = TestRepository::init_dirty()?;
		let test_hook = TestHook::init(&test_repository, HOOK_NAME)?;

		let command = test_repository.execute_cli_with(&["--pass", "--no-verify", "--", "true"]);

		command.success();
		test_hook.assert_that_hook_has_not_been_run();
		Ok(())
	}
}
