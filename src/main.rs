use clap::Parser;
use clap_complete::generate;
use std::io;
use std::process::exit;
use yansi::Paint;

mod app;
use app::App;
use app::OptionalSubcommands;

mod repository;
use repository::Repository;

mod command_choice;
use command_choice::tcr;
use command_choice::trc;
use command_choice::RepositoryCommandChoice;

mod generate_shell_completions;
use generate_shell_completions::generate_shell_completions;

fn main() -> io::Result<()> {
	#[cfg(feature = "with_log")]
	pretty_env_logger::init();

	let opt = App::parse();
	log::info!("{:#?}", opt);

	if let Some(subcommand_opt) = opt.optional_subcommands {
		match subcommand_opt {
			OptionalSubcommands::GenerateShellCompletions(generate_shell_completions_opt) => {
				generate_shell_completions(|package_name, command| {
					generate(
						generate_shell_completions_opt.shell,
						command,
						package_name,
						&mut io::stdout(),
					)
				})
			}
		}
		exit(0)
	}

	let gamble_strategy = if opt.pass { tcr } else { trc };

	let test_command = reformat_command(opt.test_command);

	if test_command.is_none() {
		log::error!("Can't parse test command");
		exit(1)
	}

	let repository = Repository::new(opt.repository_path.as_path(), opt.dry_run, opt.no_verify);

	repository.run_hook("pre-gamble", &[if opt.pass { "pass" } else { "fail" }])?;

	let command_choice = gamble_strategy(test_command.unwrap());

	match command_choice {
		RepositoryCommandChoice::ShouldCommit => {
			log::info!("commit");

			repository.command(&["add", "--all"])?;

			let base_options = vec!["commit", "--allow-empty-message"];

			let message_options = if opt.message.is_empty() && repository.head_is_failing_ref() {
				vec!["--reuse-message", "HEAD"]
			} else {
				vec!["--message", &opt.message]
			};

			let edit_options = if opt.edit {
				vec!["--edit"]
			} else {
				vec!["--no-edit"]
			};

			let fixup_options = match &opt.fixup {
				Some(commit) => vec!["--fixup", commit],
				None => vec![],
			};

			let squash_options = match &opt.squash {
				Some(commit) => vec!["--squash", commit],
				None => vec![],
			};

			let amend_options = if repository.head_is_failing_ref() {
				vec!["--amend"]
			} else {
				vec![]
			};

			let no_verify_options = if opt.no_verify {
				vec!["--no-verify"]
			} else {
				vec![]
			};

			repository.command(
				&[
					base_options,
					message_options,
					edit_options,
					fixup_options,
					squash_options,
					amend_options,
					no_verify_options,
				]
				.concat(),
			)?;

			if opt.fail {
				repository.command(&["update-ref", "refs/gamble-is-failing", "@"])?;
			}

			repository.run_hook(
				"post-gamble",
				&[
					if opt.pass { "pass" } else { "fail" },
					if opt.pass { "pass" } else { "fail" },
				],
			)?;

			log::info!("committed");
			println!("{}", Paint::blue("Committed!").bold());
		}
		RepositoryCommandChoice::ShouldRevert => {
			log::info!("revert");

			repository.command(&["reset", "--hard"])?;

			repository.run_hook(
				"post-gamble",
				&[
					if opt.pass { "pass" } else { "fail" },
					if opt.pass { "fail" } else { "pass" },
				],
			)?;

			log::info!("reverted");
			println!("{}", Paint::red("Reverted!").bold());
		}
	};

	Ok(())
}

fn reformat_command(command: Vec<String>) -> Option<Vec<String>> {
	if command.len() == 1 {
		shlex::split(command[0].as_str())
	} else {
		Some(command)
	}
}
