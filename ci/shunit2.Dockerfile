FROM alpine:3.16

RUN apk add \
	coreutils \
	git \
	bash \
	shunit2

USER 1000

CMD shunit2
