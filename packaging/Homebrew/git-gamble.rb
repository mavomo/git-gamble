class GitGamble < Formula
  desc "blend TCR + TDD to make sure to develop the right thing, babystep by babystep"
  homepage "https://gitlab.com/pinage404/git-gamble"
  head "https://gitlab.com/pinage404/git-gamble.git"

  depends_on "rust" => :build

  def install
    ENV["SHELL_COMPLETIONS_DIR"] = buildpath/"shell_completions"
    system "cargo", "install", "--locked", "--root", prefix, "git-gamble"

    bash_completion.install "#{ENV["SHELL_COMPLETIONS_DIR"]}/git-gamble.bash"
    fish_completion.install "#{ENV["SHELL_COMPLETIONS_DIR"]}/git-gamble.fish"
    zsh_completion.install "#{ENV["SHELL_COMPLETIONS_DIR"]}/_git-gamble"
  end

  test do
    system "git", "init"
    system "git", "config", "user.name", "Git Gamble"
    system "git", "config", "user.email", "git@gamble"

    system "sh", "-c", "echo 'failing' > 'some_file'"
    system "#{bin}/git-gamble", "--fail", "--", "false"
    assert_equal "failing", shell_output("cat some_file").strip

    system "sh", "-c", "echo 'should pass but still failing' > 'some_file'"
    system "#{bin}/git-gamble", "--pass", "--", "false"
    assert_equal "failing", shell_output("cat some_file").strip

    system "sh", "-c", "echo 'passing' > 'some_file'"
    system "#{bin}/git-gamble", "--pass", "--", "true"
    assert_equal "passing", shell_output("cat some_file").strip
  end
end
