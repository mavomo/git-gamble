{
  inputs = {
    home-manager.url = "github:nix-community/home-manager/release-21.11";

    git-gamble.url = "gitlab:pinage404/git-gamble";
  };

  outputs = { self, home-manager, git-gamble }: {
    homeConfigurations = {
      jdoe = home-manager.lib.homeManagerConfiguration {
        system = "x86_64-linux";
        homeDirectory = "/home/jdoe";
        username = "jdoe";
        configuration = {
          imports = [ ./home.nix ];
          nixpkgs.overlays = [
            git-gamble.overlay
          ];
        };
      };
    };
  };
}
