let
  nixpkgs = builtins.fetchTarball https://github.com/NixOS/nixpkgs/archive/568e0bc498ee51fdd88e1e94089de05f2fdbd18b.tar.gz;
  pkgs = import nixpkgs { };
  nixFlakes = pkgs.writeShellScriptBin "nix" ''
    exec ${pkgs.nixUnstable}/bin/nix --experimental-features "nix-command flakes" "$@"
  '';
in
pkgs.mkShell {
  packages = [
    pkgs.home-manager
    nixFlakes
  ];
}
