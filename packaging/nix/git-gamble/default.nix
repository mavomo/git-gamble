{ version ? "2.5.0"
, sha256 ? "1111111111111111111111111111111111111111111111111111"
, cargoSha256 ? "0000000000000000000000000000000000000000000000000000"

, rustPlatform
, fetchFromGitLab
, git
, stdenv
, installShellFiles
}:

rustPlatform.buildRustPackage rec {
  pname = "git-gamble";

  inherit version cargoSha256;

  src = fetchFromGitLab {
    owner = "pinage404";
    repo = pname;
    rev = "version/${version}";
    inherit sha256;
  };

  # git is needed at runtime
  # don't know which of these "propagated" values should be used, both of them works:
  # * depsBuildBuildPropagated
  # * propagatedNativeBuildInputs
  # * depsBuildTargetPropagated
  # * depsHostHostPropagated
  # * propagatedBuildInputs
  # * depsTargetTargetPropagated
  depsTargetTargetPropagated = [ git ];

  # install shell completion
  nativeBuildInputs = [ installShellFiles ];
  postInstall = ''
    installShellCompletion $releaseDir/build/git-gamble-*/out/{_git-gamble,git-gamble.{bash,fish}}

    PATH="$PATH:$out/bin/" ./script/usage.sh > git-gamble.1
    installManPage git-gamble.1
  '';

  meta = {
    description = "blend TCR + TDD to make sure to develop the right thing, babystep by babystep";
    homepage = "https://gitlab.com/pinage404/git-gamble";
    changelog = "https://gitlab.com/pinage404/git-gamble/-/blob/main/CHANGELOG.md";
    license = stdenv.lib.licenses.isc;
  };
}
