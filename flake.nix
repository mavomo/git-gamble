{
  # broken on Fish
  # as a workaround, use `nix develop` the first time
  # https://github.com/direnv/direnv/issues/1022
  nixConfig.extra-substituters = [ "https://git-gamble.cachix.org" ];
  nixConfig.extra-trusted-public-keys = [ "git-gamble.cachix.org-1:afbVJAcYMKSs3//uXw3HFdyKLV66/KvI4sjehkdMM/I=" ];

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    flake-utils.url = "github:numtide/flake-utils";

    # used only for retro compatibility with `nix-shell`
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    crane = {
      url = "github:ipetkov/crane";
      inputs.flake-compat.follows = "flake-compat";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.rust-overlay.follows = "rust-overlay";
    };
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
    , flake-compat
    , rust-overlay
    , crane
    }:
    # uncomment to debug with `traceVal someValue` `traceSeq someValue` `traceValSeq someValue` `traceValSeqN 2 someValue`
    # with import ./nix/debug.nix { inherit nixpkgs; };
    (flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [
          rust-overlay.overlays.default
        ];
      };

      rust-toolchain = (pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain.toml);

      devShell = pkgs.mkShell {
        packages = [
          rust-toolchain
          (import ./nix/shell_packages.nix {
            pkgs = pkgs;
          })
        ];
      };

      craneLib = (crane.mkLib pkgs).overrideToolchain rust-toolchain;

      src = craneLib.cleanCargoSource ./.;

      rust-dependencies = craneLib.buildDepsOnly {
        inherit src;
      };

      rust-package-binary = craneLib.buildPackage {
        pname = "git-gamble-binary";
        inherit src;
        cargoArtifacts = rust-dependencies;

        doCheck = false;
      };

      package = craneLib.buildPackage {
        src = pkgs.lib.sources.cleanSource ./.;
        cargoArtifacts = rust-package-binary;

        doCheck = false;

        buildInputs = [
          pkgs.git
        ];

        nativeBuildInputs = [
          pkgs.installShellFiles
        ];
        postInstall = ''
          installShellCompletion --fish target/release/build/git-gamble-*/out/git-gamble.fish
          installShellCompletion --bash target/release/build/git-gamble-*/out/git-gamble.bash
          installShellCompletion --zsh target/release/build/git-gamble-*/out/_git-gamble

          PATH="$PATH:target/release/bin/" sh ./script/usage.sh > git-gamble.1
          installManPage git-gamble.1
        '';
      };

      app = flake-utils.lib.mkApp {
        drv = package;
      };
    in
    {
      # run with `nix develop`
      devShells.default = devShell;

      # run with `nix shell`
      packages.git-gamble-deps = rust-dependencies;
      packages.git-gamble-binary = rust-package-binary;
      packages.git-gamble = package;
      packages.default = package;

      # run with `nix run`
      apps.git-gamble = app;
      apps.default = app;

      # run with `nix fmt`
      formatter = pkgs.nixpkgs-fmt;

      overlays.default = import ./nix/overlay.nix;
      overlay = self.overlays.default;
    }
    ))
  ;
}
